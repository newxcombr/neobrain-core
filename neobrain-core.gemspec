# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'neobrain/core/version'

Gem::Specification.new do |spec|
  spec.name          = "neobrain-core"
  spec.version       = Neobrain::Core::VERSION
  spec.authors       = ["Newton Garcia"]
  spec.email         = ["newxhost@gmail.com"]
  spec.description   = %q{Neobrain core}
  spec.summary       = %q{Neobrain core}
  spec.homepage      = "http://neobrain.com.br"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"

  spec.add_dependency "decisiontree"
  spec.add_dependency "graphr"
  spec.add_dependency "ai4r"
end
