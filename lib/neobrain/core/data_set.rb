module Neobrain
  module Core

    class DataSet

      attr_accessor :name
      attr_accessor :datasource
      attr_accessor :objective
      attr_accessor :objective_index
      attr_accessor :attributes
      attr_accessor :datasource_attributes
      attr_accessor :categories
      attr_accessor :instances
      # Data type: continuous or discrete
      attr_accessor :datatype



      def initialize
        @datasource_attributes = []
        @attributes = []
        @categories = []
        @instances = []
        @datatype = :continuous #:discrete
      end

      def set_default_objective
        @objective = @attributes.last
      end

      def set_objective(objective)
        @objective = objective
      end

      def get_objective_index
        @objective_index = @datasource_attributes.index(@objective)
      end

      def load(datasource)
        raise TypeError, "DataSet.load only accepts DataSource object as argument" if !datasource.kind_of?(DataSource)

        @datasource = datasource
        @datasource_attributes = datasource.attributes
        get_objective_index

        #remove the objective attribute from attributes
        datasource.attributes.pop
        @attributes = datasource.attributes

        raise Neobrain::Core::DataSet::CategoriesNotFound if datasource.attributes.empty?
        raise Neobrain::Core::DataSet::ObjectiveIndexNotFound if @objective_index.nil? or @objective_index < 1

        datasource.instances.each do |instance|
          update_categories(instance)
          @instances << instance
        end

      end

      def update_categories(instance)
        @categories << instance[@objective_index] if !@categories.include?(instance[@objective_index])
      end

      def to_s
        "<DataSet @datasource_attributes=#{@datasource_attributes} @objective='#{@objective}' @size=#{size} @datasource='#{@datasource.resource}'>"
      end

      def size
        @instances.size
      end

    end
  end
end


class Neobrain::Core::DataSet::CategoriesNotFound < StandardError; end
class Neobrain::Core::DataSet::ObjectiveIndexNotFound < StandardError; end
