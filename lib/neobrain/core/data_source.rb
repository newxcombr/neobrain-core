$app_path = File.dirname(File.dirname(File.expand_path(__FILE__)))
require 'csv'

module Neobrain
  module Core

    class DataSource

      attr_accessor :ds_type
      attr_accessor :driver
      attr_accessor :resource
      attr_accessor :url
      attr_accessor :instances
      attr_accessor :attributes

      def initialize(ds_type)
        @ds_type = ds_type
        @instances = []
      end

      def load(resource, params = {})
        @resource = resource

        case @ds_type
        when :csv then @driver = DataSource::CSV.new
        when :array then @driver = DataSource::Array.new
        end

        @instances = @driver.load(resource, params)
        @attributes = @driver.attributes

        @instances
      end

    end


    class DataSource::CSV
      attr_accessor :items
      attr_accessor :resource
      attr_accessor :attributes

      def initialize
        @items = []
      end

      def load(resource, params = {})
        @resource = resource
        @attributes = parse_headers

        opts = {:headers => true, :return_headers => false}
        CSV.foreach(resource, opts) do |row|
          @items << parse_row(row.to_csv.parse_csv)
        end

        @items
      end

      def parse_headers
        headers_str = File.open(@resource, &:readline)
        headers = headers_str.gsub('"','').split(",").map(&:strip).map{|s| s.parameterize }

        headers
      end

      def parse_row(array)
        array.map {|value| value.is_number? ? value.to_f : value.to_s }
      end

    end


    class DataSource::Array
      attr_accessor :items
      attr_accessor :attributes

      def initialize
        @items = []
      end

      def load(items, params)
        @attributes = params[:attributes]
        @items = items.map{|row| parse_row(row) }
      end

      def parse_row(array)
        array.map {|value| value.is_number? ? value.to_f : value.to_s }
      end

    end
  end

end
