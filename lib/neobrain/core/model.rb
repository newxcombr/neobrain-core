# -*- encoding : utf-8 -*-
require "benchmark"

module Neobrain
  module Core

    class Model

      attr_accessor :name
      attr_accessor :dataset
      attr_accessor :classifier

      # Defines what kind of prediction we expect from it (classification, clustering)
      attr_accessor :type

      @@storage_path = nil

      def initialize
        #app_path = File.dirname(File.dirname(File.expand_path(__FILE__)))
        @@storage_path = "/tmp"
      end

      def load(dataset)
        @dataset = dataset
      end

      def train
        time = Benchmark.realtime do
          # Instantiate the tree, and train it based on the data (set default to '1')
          @classifier = DecisionTree::ID3Tree.new(@dataset.attributes, @dataset.instances, nil, @dataset.datatype)
          @classifier.train
        end

        { size: @dataset.instances.size, time: time }
      end

      def prediction(instance)
        @classifier.predict(instance)
      end

      def to_graph(file_path)
        @classifier.graph(file_path)
      end

      def get_rules
        @classifier.ruleset.rules
      end

      def rules
        build_rules
      end

      def build_rules
        rules_code = ""
        @classifier.ruleset.rules.each do |rule|
          conditions = []
          prediction = rule.conclusion.is_number? ? rule.conclusion : "'#{rule.conclusion}'"

          if rules_code.empty? then
            rules_code << "  if "
          else
            rules_code << "  elsif "
          end

          rule.premises.each do |item|
            node, operator = item
            ident = "    " if !conditions.empty?
            conditions << "#{ident}#{node.attribute} #{operator} #{node.threshold}"
          end

          conditions_str = conditions.join(" and \n")
          rules_code << "(#{conditions_str})"
          rules_code << "\n    then #{prediction}\n"
        end

        rules_code << "  else false \n"
        rules_code << "  end"

        rules_code
      end

      def rules_as_method
        args = @dataset.attributes.join(", ")
        method_str = "def classify(#{args}) \n"
        method_str << "#{build_rules} \n"
        method_str << "end"
      end

      def save(model_id, path = nil)
        dir = path.nil? ? "/tmp" : path
        serialized_object = Marshal.dump(self).force_encoding("UTF-8")
        file_path = File.join dir, "#{model_id}.model"
        File.open(file_path, 'w') {|f| f.write(serialized_object) }
      end

      def self.restore(model_id = nil, path = nil)
        if File.exists?("#{model_id}") then
          file_path = "#{model_id}"
        else
          dir = path.nil? ? "/tmp" : path
          file_path = File.join dir, "#{model_id}.model"
        end

        serialized_object = File.read(file_path)
        model = Marshal.load(serialized_object)

        model
      end

      def self.exists?(model_id, path = nil)
        dir = path.nil? ? "/tmp" : path
        File.exists?("#{dir}/#{model_id}.model")
      end

    end
  end
end
