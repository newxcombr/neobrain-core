require "neobrain/core/version"

require 'neobrain/core/model'
require 'neobrain/core/data_set'
require 'neobrain/core/data_source'

require 'decisiontree'
require 'graphr'
require "ai4r"

module Neobrain
  module Core

  end
end



class String
  def parameterize
    if !self.nil? then
      self.gsub(" ", "-").gsub(".", "-").gsub("--", "-").downcase
    end
  end
end


class Object
  def is_number?
    self.to_f.to_s == self.to_s || self.to_i.to_s == self.to_s
  end
end

class Numeric
  def percent_of(n)
    self.to_f / n.to_f * 100.0
  end
end


#class Object
#  def save_to_file(filename)
#    File.open(filename, 'w+' ) { |f| f << Marshal.dump(self) }
#  end
#
#  def self.load_from_file(filename)
#    Marshal.load( File.read( filename ) )
#  end
#end
