require 'spec_helper'

describe Neobrain::Core::Model do
  it "prediction with continuous data" do

    data_source = Neobrain::Core::DataSource.new(:csv)
    data_source.load("#{$gem_root}/spec/data/iris_training.csv")

    dataset = Neobrain::Core::DataSet.new
    dataset.set_objective("species")
    dataset.load(data_source)

    model = Neobrain::Core::Model.new
    model.load(dataset)
    model.train

    model.prediction([5.4,3.4,1.5,0.4]).should eql("setosa")
    model.prediction([6.0,3.4,4.5,1.6]).should eql("versicolor")
    model.prediction([6.4,3.2,5.3,2.3]).should eql("virginica")

    filepath = "/tmp/test_graph"
    model.to_graph(filepath)
    File.exists?("#{filepath}.png").should == true

  end

  it "prediction with discrete data" do
    data_source = Neobrain::Core::DataSource.new(:csv)
    data_source.load("#{$gem_root}/spec/data/buyers_discrete_training.csv")

    dataset = Neobrain::Core::DataSet.new
    dataset.set_objective("buyer")
    dataset.datatype = :discrete
    dataset.load(data_source)

    model = Neobrain::Core::Model.new
    model.load(dataset)

    puts "\n"
    puts dataset.to_s

    model.train

    model.prediction(["36 - 55", "masters",      "high", "single"]).should eql("will-buy")
    model.prediction(["18 - 35", "high school",  "low",  "single"]).should eql("wont-buy")
    model.prediction(["18 - 35", "masters",      "high", "single"]).should eql("wont-buy")
    model.prediction(["36 - 55", "high school",  "low",  "single"]).should eql("will-buy")

    #puts model.rules
    #puts model.rules_as_method
  end

  it "prediction avg" do

    data_source = Neobrain::Core::DataSource.new(:csv)
    data_source.load("#{$gem_root}/spec/data/iris_training.csv")

    dataset = Neobrain::Core::DataSet.new
    dataset.set_objective("species")
    dataset.load(data_source)

    model = Neobrain::Core::Model.new
    model.load(dataset)
    model.train

    #PREDICTIONS
    data_source_test = Neobrain::Core::DataSource.new(:csv)
    data_source_test.load("#{$gem_root}/spec/data/iris_test.csv")

    assertions = 0
    errors = 0
    total_instances = data_source_test.instances.size

    data_source_test.instances.each do |instance|
      expected_category = instance.pop
      category = model.prediction(instance)

      category == expected_category ? assertions += 1 : errors += 1
    end

    puts "\n PREDICTIONS RESULTS"
    puts "-"*160
    puts "Assertions: #{assertions.percent_of(total_instances).round(2)}% of #{total_instances}"
    puts "Errors: #{errors.percent_of(total_instances).round(2)}% of #{total_instances}"
    puts "-"*160

  end

  it "should have persistence" do

    data_source = Neobrain::Core::DataSource.new(:csv)
    data_source.load("#{$gem_root}/spec/data/iris_training.csv")

    dataset = Neobrain::Core::DataSet.new
    dataset.set_objective("species")
    dataset.load(data_source)

    model = Neobrain::Core::Model.new
    model.load(dataset)
    model.train

    model_id = "iris_classifier"
    model.save(model_id)
    Neobrain::Core::Model.exists?(model_id).should be true

    model = nil

    model_restored = Neobrain::Core::Model.restore(model_id, "/tmp")
    model_restored.prediction([6.0,3.4,4.5,1.6]).should == "versicolor"

  end

  it "should export rules" do

    attributes = ["SepalLength", "SepalWidth", "PetalLength", "PetalWidth", "species"]
    training = [
      [5.1,3.5,1.4,0.2,"setosa"],
      [4.9,3,1.4,0.2,"setosa"],
      [4.7,3.2,1.3,0.2,"setosa"],
      [4.6,3.1,1.5,0.2,"setosa"],
      [5.5,2.4,3.7,1,"versicolor"],
      [5.8,2.7,3.9,1.2,"versicolor"],
      [6,2.7,5.1,1.6,"versicolor"],
      [5.4,3,4.5,1.5,"versicolor"],
      [6,3.4,4.5,1.6,"versicolor"],
      [6.7,3.1,4.7,1.5,"versicolor"],
      [6.3,2.3,4.4,1.3,"versicolor"],
      [5.6,3,4.1,1.3,"versicolor"],
      [5.5,2.5,4,1.3,"versicolor"],
      [5.5,2.6,4.4,1.2,"versicolor"],
      [6.2,2.8,4.8,1.8,"virginica"],
      [6.1,3,4.9,1.8,"virginica"],
      [6.4,2.8,5.6,2.1,"virginica"],
      [7.2,3,5.8,1.6,"virginica"],
      [7.4,2.8,6.1,1.9,"virginica"],
      [7.9,3.8,6.4,2,"virginica"],
    ]

    data_source = Neobrain::Core::DataSource.new(:array)
    data_source.load(training, {attributes: attributes})

    dataset = Neobrain::Core::DataSet.new
    dataset.set_objective("species")
    dataset.load(data_source)

    model = Neobrain::Core::Model.new
    model.load(dataset)
    model.train

    model.prediction([6.0,3.4,4.5,1.6]).should == "versicolor"

    code_expected = "def classify(SepalLength, SepalWidth, PetalLength, PetalWidth) \nif (SepalLength >= 5.25 and \nPetalLength >= 4.75 and \nSepalLength >= 6.05)\nthen 'virginica'\nelsif (SepalLength >= 5.25 and \nPetalLength >= 4.75 and \nSepalLength < 6.05)\nthen 'versicolor'\nelsif (SepalLength >= 5.25 and \nPetalLength < 4.75)\nthen 'versicolor'\nelsif (SepalLength < 5.25)\nthen 'setosa'\nelse false \nend \nend"
    model.rules_as_method.gsub(/^( |\t)+/, "").should == code_expected.gsub(/^( |\t)+/, "")

  end

end
