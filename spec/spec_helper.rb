require_relative '../lib/neobrain/core'
require_relative '../lib/neobrain/core/model'
require_relative '../lib/neobrain/core/data_set'
require_relative '../lib/neobrain/core/data_source'

$gem_root = Gem.loaded_specs['neobrain-core'].full_gem_path
