require 'spec_helper'

describe Neobrain::Core::DataSet do
  it "should load a file" do

    data_source = Neobrain::Core::DataSource.new(:csv)
    data_source.load("#{$gem_root}/spec/data/iris_training.csv")

    ds = Neobrain::Core::DataSet.new
    ds.set_objective("species")
    ds.load(data_source)

    ds.instances.size.should eql(149)

    expected_categories = ["setosa", "versicolor", "virginica"]
    expected_categories.should eql(ds.categories)

  end
end
