require 'spec_helper'

describe Neobrain::Core::DataSource do
  it "should load a file" do
    ds = Neobrain::Core::DataSource.new(:csv)
    ds.load("#{$gem_root}/spec/data/iris_training.csv")
    ds.instances.size.should eql(149)
  end
end
